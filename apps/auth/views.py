from django.contrib.auth.signals import user_logged_in
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework import permissions, status
from knox.models import AuthToken
from knox.views import LoginView as KnoxLoginView
from knox.settings import knox_settings
import datetime
from simple_asym.crypto import verify_owner

from apps.access.models import ServerKeyPair
from .authentication import QuietBasicAuthentication
from .serializers import CodeSerializer
import base64


class LoginView(KnoxLoginView):
    authentication_classes = [QuietBasicAuthentication]

    def post(self, request, format=None):
        """ Accepts optional param - expires - int - number of hours until token expires """
        expires = request.data.get('expires', None)
        # Default to global settings
        if expires is None: 
            return super().post(request, format)

        try:
            expires = datetime.timedelta(hours=int(expires))
        except ValueError:
            raise ValidationError("expires must a number")
        token = AuthToken.objects.create(request.user, expires=expires)
        user_logged_in.send(sender=request.user.__class__, request=request, user=request.user)
        UserSerializer = knox_settings.USER_SERIALIZER
        context = {'request': self.request, 'format': self.format_kwarg, 'view': self}
        return Response({
            'user': UserSerializer(request.user, context=context).data,
            'token': token,
        })


class BackupCodeLoginView(KnoxLoginView):
    """ Backup code driven method of authentications.
    Requires:
    
    - Proof of private key ownership
    - Email backup code token
    """
    permission_classes = [permissions.AllowAny]

    def post(self, request, format=None):
        serializer = CodeSerializer(data=request.data)
        if serializer.is_valid():
            User = get_user_model()
            
            # Confirm reset password token (again)
            user = User.objects.filter(
                email=serializer.validated_data['email'],
                backupcodeemailtoken__code=serializer.validated_data['code'],
                backupcodeemailtoken__expiration__gte = timezone.now()
            ).first()
            if not user:
                return Response(status=status.HTTP_401_UNAUTHORIZED)

            # Check message if sent from claimed key pair
            message = serializer.validated_data['message']
            public_key = base64.b64decode(user.public_key)
            private_key = base64.b64decode(ServerKeyPair.objects.first().private_key)
            # One of very few places the server uses crypto
            # Verify the message came from the owner of the user's public key
            if not verify_owner(public_key, private_key, message):
                return Response(status=status.HTTP_401_UNAUTHORIZED)

            token = AuthToken.objects.create(user)
            user_logged_in.send(sender=User, request=request, user=user)
            UserSerializer = knox_settings.USER_SERIALIZER
            context = {'request': self.request, 'format': self.format_kwarg, 'view': self}
            return Response({
                'user': UserSerializer(user, context=context).data,
                'token': token,
            })
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
