from django.core.urlresolvers import reverse
from django.test import override_settings
from rest_framework.test import APITestCase
from passit.test_client import PassitTestMixin
from model_mommy import mommy
from ..models import User


class TestUsersAPI(PassitTestMixin, APITestCase):
    def test_users_opt_in_error_reporting(self):
        url = reverse('users-list')
        user = mommy.make(User)
        self.client.force_login(user)
        res = self.client.get(url)
        self.assertEqual(res.data[0]['opt_in_error_reporting'], False)

    @override_settings(ERROR_REPORT_DEFAULT=True)
    def test_users_default_opt_in_error_reporting(self):
        email = "testy@test.test"
        password = "12345678"
        self.sdk.sign_up(email, password)

        user = User.objects.get()
        url = reverse('users-list')
        self.client.force_login(user)
        res = self.client.get(url)
        self.assertEqual(res.data[0]['opt_in_error_reporting'], True)

    def test_users_patch_default_opt_in_error_reporting(self):
        user = mommy.make(User)
        url = reverse('users-detail', args=[user.id])
        self.client.force_login(user)
        data = {
            'opt_in_error_reporting': True,
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 200)
        user.refresh_from_db()
        self.assertEqual(user.opt_in_error_reporting, True)

    def test_users_patch_email(self):
        """ Users are not allowed to change email """
        user = mommy.make(User)
        user_email = user.email
        url = reverse('users-detail', args=[user.id])
        self.client.force_login(user)
        data = {
            'email': 'trick@example.com',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 200)
        user.refresh_from_db()
        self.assertEqual(res.data['email'], user_email)
        self.assertEqual(user.email, user_email)

    def test_users_patch_password(self):
        """ Users are not allowed to change their password without entering 
        their current password """
        user = mommy.make(User)
        user.set_password('hunter2')
        user.save()
        user.refresh_from_db()
        old_password = user.password
        url = reverse('users-detail', args=[user.id])
        self.client.force_login(user)

        data = {
            'password': 'somethingbad',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 400)

        data = {
            'password': 'somethingbad',
            'current_password': 'wrong',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 400)
        user.refresh_from_db()
        self.assertEqual(user.password, old_password)

        data = {
            'password': 'somethingbad',
            'current_password': 'hunter2',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 200)
        user.refresh_from_db()
        self.assertNotEqual(user.password, old_password)
