# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-29 19:30
from __future__ import unicode_literals

from django.db import migrations, models


def all_admin(apps, schema_editor):
    GroupUser = apps.get_model('access', 'GroupUser')
    GroupUser.objects.all().update(is_group_admin=True)

class Migration(migrations.Migration):

    dependencies = [
        ('access', '0005_user_email_confirmed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='groupuser',
            name='is_group_admin',
            field=models.BooleanField(default=True, help_text='Is able to add/remove members'),
        ),
        migrations.RunPython(all_admin),
    ]
